import React, { Component } from 'react';

class Store extends Component {
    render() {
        return (
            <section className="store_home">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-xs-12">
                            <h2 className="store_home_title line_after_heading section_heading">cửa hàng</h2>
                        </div>
                        <div className="col-lg-12 col-md-12 col-xs-12">
                            <div className="store_slider">
                                <div className="store_slider_item display_flex">
                                    <div className="store_slider_content justify-content-center display_flex store_slider_col">
                                        <p className="store_slider_title">THE COFFEE HOUSE SIGNATURE</p>
                                        <p className="store_slider_des">Với những nghệ nhân rang tâm huyết và đội ngũ Barista
                                          tài năng cùng những câu chuyện cà phê đầy cảm hứng, ngôi nhà Signature 19B Phạm
                                          Ngọc Thạch, Q.3, TP.HCM là không gian dành riêng cho những ai trót yêu say đắm
                hương vị của những hạt cà phê tuyệt hảo.</p>
                                    </div>
                                    <div className="store_slider_img store_slider_col">
                                        <img src="img/article/3e0a8783_master.jpg" alt="Store Slider 1" />
                                    </div>
                                </div>
                            </div>
                            <div className="store_slider_2 owl-carousel" />
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Store;