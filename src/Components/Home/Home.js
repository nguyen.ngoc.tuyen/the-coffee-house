import React, { Component } from 'react';
import Blog from './Blog';
import Store from './Store';
import Insta from './Insta';
import HomeMenu from './HomeMenu';
import HomeSlide from './HomeSlide';

class Home extends Component {
    render() {
        return (
            <div className="main">
                <HomeSlide></HomeSlide>
                <HomeMenu></HomeMenu>
                <Blog></Blog>
                <Store></Store>
                <Insta></Insta>
            </div>
        );
    }
}

export default Home;