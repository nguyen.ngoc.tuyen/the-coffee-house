import React, { Component } from 'react';
import { Link } from "react-router-dom";
import MenuItem from '../Menu/MenuProduct/MenuItem';
import { connect } from 'react-redux';

class HomeMenu extends Component {
    getProductRandom = () => {
        // console.log(this.props.productData);
        
        var count = 0
        return this.props.productData.map((value, key) => {
            
            var random = this.props.productData[Math.floor(Math.random() * this.props.productData.length)];
            // console.log(random);
            if (count <= 2) {
                count++
                return (
                    <MenuItem
                        key={key}
                        product={random}
                        productId={random.productId}
                        productTitle={random.productTitle}
                        productImage={random.productImage}
                        productPrice={random.productPrice}
                        productTitleDes={random.productTitleDes}
                        productDescription={random.productDescription}
                        productCategory={random.productCategory}
                    />
                )
            }
            return true
        });
    }
    render() {
        return (
            <section className="menu_home">
                <svg className="menu_bg_top">
                    <use xmlnsXlink="http://www.w3.org/1999/xlink" xlinkHref="#bg_1553" />
                </svg>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-xs-12">
                            <h2 className="menu_home_title line_after_heading section_heading">Menu</h2>
                            <div className="viewmore_menu_home"><Link to="/menu" className="animate_btn" title="xem thêm tất cả sản phẩm">xem thêm tất cả sản phẩm</Link></div>
                        </div>
                        <div className="clearfix" />
                        <div className="menu_list_home flex_wrap display_flex">
                            {this.getProductRandom()}
                        </div>
                    </div>
                </div>
                <svg className="menu_bg_bottom">
                    <use xmlnsXlink="http://www.w3.org/1999/xlink" xlinkHref="#bg_1553" />
                </svg>
            </section>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        productData: state.productData
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeMenu);