import React, { Component } from 'react';

class Blog extends Component {
  render() {
    return (
      <section className="blog_home">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-xs-12">
              <h2 className="blog_home_title line_after_heading section_heading">Blog</h2>
              <div className="viewmore_blog_home"><a className="animate_btn" href="blogs/news" title="XEM THÊM">XEM
            THÊM</a></div>
            </div>
            <div className="clearfix" />
            <div className="blog_lists flex_wrap display_flex">
              <div className="top_blog_home blog_item flex_direction_column display_flex"><a className="article_item_image" href="/blogs/news/le-hoi-macchiato-them-cam-hung-them-vui"><div className="article_img" style={{ backgroundImage: 'url(//file.hstatic.net/1000075078/article/cover_fb_800x300_c97ded2d90324c2486b5954c37b6bdc5_1024x1024.jpg)' }} /></a>
                <div className="article_item_info">
                  <span className="article_tags">Tin tức</span>
                  <h3><a href="/blogs/news/le-hoi-macchiato-them-cam-hung-them-vui">LỄ HỘI MACCHIATO - THÊM CẢM HỨNG, THÊM VUI!</a></h3>
                  <div className="meta"><time>-3 ngày trước</time></div>
                  <div className="short_des">Đến hẹn lại lên, Mùa Lễ Hội Macchiato đã quay trở lại! Hãy sẵn sàng xóa tan cơn nắng mưa thất thường, để hòa mình vào Lễ Hội Macchiato đầy...</div>
                  <div className="article_item_action">
                    <a className="animate_btn" href="/blogs/news/le-hoi-macchiato-them-cam-hung-them-vui">Xem thêm</a>
                  </div>
                </div>
              </div>
              <div className="blog_item flex_direction_column display_flex "><a className="article_item_image" href="blogs/news/chuong-trinh-khach-hang-than-thiet-va-nhung-cau-hoi-thuong-gap" title="GIẢI ĐÁP THẮC MẮC CỦA BẠN VỚI CHƯƠNG TRÌNH KHÁCH HÀNG THÂN THIẾT MỚI">
                <div className="article_img" style={{ backgroundImage: 'url(img/article/tch_video_design_02_be33c39f38294d0380014fbdcdc135e7_grande.jpg)' }}>
                </div>
              </a>
                <div className="article_item_info display_flex flex_direction_column justify-content-spbetween">
                  <span className="article_tags">Tin tức</span>
                  <h3><a href="blogs/news/chuong-trinh-khach-hang-than-thiet-va-nhung-cau-hoi-thuong-gap" title="GIẢI ĐÁP THẮC MẮC CỦA BẠN VỚI CHƯƠNG TRÌNH KHÁCH HÀNG THÂN THIẾT MỚI">GIẢI
                ĐÁP THẮC MẮC CỦA BẠN VỚI CHƯƠNG TRÌNH KHÁCH HÀNG THÂN THIẾT MỚI</a></h3>
                  <p>Từ ngày 23/09, The Coffee House sẽ ra mắt Chương trình khách hàng thân thiết - The
              Coffee House Rewards mới thông qua ứng dụng The Coffee House, với những...</p>
                  <div className="article_item_action">
                    <a className="animate_btn" href="blogs/news/chuong-trinh-khach-hang-than-thiet-va-nhung-cau-hoi-thuong-gap">Xem
                thêm</a>
                  </div>
                </div>
              </div>
              <div className="blog_item flex_direction_column display_flex "><a className="article_item_image" href="blogs/news/chuong-trinh-khach-hang-than-thiet-moi-va-nhung-dieu-co-the-ban-chua-biet" title="CHƯƠNG TRÌNH KHÁCH HÀNG THÂN THIẾT MỚI và NHỮNG ĐIỀU CÓ THỂ BẠN CHƯA BIẾT">
                <div className="article_img" style={{ backgroundImage: 'url(img/article/_2ff56aa17382400c990f52191f208ec8_8347e74c6266406b8fc9c4706f01149b_964e0cbe297b4a78892e271fa0893373_grande.png)' }}>
                </div>
              </a>
                <div className="article_item_info display_flex flex_direction_column justify-content-spbetween">
                  <span className="article_tags">Tin tức</span>
                  <h3><a href="blogs/news/chuong-trinh-khach-hang-than-thiet-moi-va-nhung-dieu-co-the-ban-chua-biet" title="CHƯƠNG TRÌNH KHÁCH HÀNG THÂN THIẾT MỚI và NHỮNG ĐIỀU CÓ THỂ BẠN CHƯA BIẾT">CHƯƠNG
                TRÌNH KHÁCH HÀNG THÂN THIẾT MỚI và NHỮNG ĐIỀU CÓ THỂ BẠN CHƯA BIẾT</a></h3>
                  <p>Từ ngày 23/09, The Coffee House sẽ ra mắt Chương trình khách hàng thân thiết - The
              Coffee House Rewards mới thông qua ứng dụng The Coffee House, với những...</p>
                  <div className="article_item_action">
                    <a className="animate_btn" href="blogs/news/chuong-trinh-khach-hang-than-thiet-moi-va-nhung-dieu-co-the-ban-chua-biet">Xem
                thêm</a>
                  </div>
                </div>
              </div>
              <div className="blog_item flex_direction_column display_flex "><a className="article_item_image" href="blogs/news/the-coffee-house-5-nam-hoat-dong-vi-cong-dong" title="THE COFFEE HOUSE - 5 NĂM HOẠT ĐỘNG VÌ CỘNG ĐỒNG">
                <div className="article_img" style={{ backgroundImage: 'url(img/article/0914-tch-p5-800x300_c0e092305fd042399a734b725eb6ad5b_grande.jpg)' }}>
                </div>
              </a>
                <div className="article_item_info display_flex flex_direction_column justify-content-spbetween">
                  <span className="article_tags">Tin tức</span>
                  <h3><a href="blogs/news/the-coffee-house-5-nam-hoat-dong-vi-cong-dong" title="THE COFFEE HOUSE - 5 NĂM HOẠT ĐỘNG VÌ CỘNG ĐỒNG">THE COFFEE HOUSE - 5 NĂM
                HOẠT ĐỘNG VÌ CỘNG ĐỒNG</a></h3>
                  <p>Ngay từ những ngày đầu, The Coffee House luôn lấy con người làm trọng tâm cho mọi
              quyết định và sản phẩm. Chúng tôi tin rằng, một không gian cà...</p>
                  <div className="article_item_action">
                    <a className="animate_btn" href="blogs/news/the-coffee-house-5-nam-hoat-dong-vi-cong-dong">Xem thêm</a>
                  </div>
                </div>
              </div>
              <div className="blog_item flex_direction_column display_flex "><a className="article_item_image" href="blogs/news/the-coffee-house-nam-thu-5-br-moi-ngay-deu-la-mot-ngay-hanh-phuc" title="THE COFFEE HOUSE NĂM THỨ 5 MỖI NGÀY ĐỀU LÀ MỘT NGÀY HẠNH PHÚC">
                <div className="article_img" style={{ backgroundImage: 'url(img/article/kv_800x300_e6b18a2718ad451a9730c61f373070d5_grande.jpg)' }}>
                </div>
              </a>
                <div className="article_item_info display_flex flex_direction_column justify-content-spbetween">
                  <span className="article_tags">Tin tức</span>
                  <h3><a href="blogs/news/the-coffee-house-nam-thu-5-br-moi-ngay-deu-la-mot-ngay-hanh-phuc" title="THE COFFEE HOUSE NĂM THỨ 5 MỖI NGÀY ĐỀU LÀ MỘT NGÀY HẠNH PHÚC">THE COFFEE
                HOUSE NĂM THỨ 5 MỖI NGÀY ĐỀU LÀ MỘT NGÀY HẠNH PHÚC</a></h3>
                  <p>Mỗi ngày tại The Coffee House, chúng tôi đều hy vọng hôm nay sẽ làm được 2 điềuGửi
              trao những tách cà phê chất lượng và Lan tỏa hạnh phúc...</p>
                  <div className="article_item_action">
                    <a className="animate_btn" href="blogs/news/the-coffee-house-nam-thu-5-br-moi-ngay-deu-la-mot-ngay-hanh-phuc">Xem
                thêm</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Blog;