import React, { Component } from 'react';

class HomeSlide extends Component {
    render() {
        return (
            <section id="myCarousel" className="carousel slide" data-ride="carousel">
                {/* Indicators */}
                <ol className="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to={0} className="active" />
                    <li data-target="#myCarousel" data-slide-to={1} />
                    <li data-target="#myCarousel" data-slide-to={2} />
                </ol>
                {/* Wrapper for slides */}
                <div className="carousel-inner">
                    <div className="item active">
                        <img src="http://file.hstatic.net/1000075078/file/kv_1920x768_web_43f74ec213d44d0fb5b8f9a25f6e3ee0_master.jpg" alt="" />
                    </div>
                    <div className="item">
                        <img className="img-fluid" src="https://file.hstatic.net/1000075078/file/cover_1920_x_768_mac__67a330c51b0c40fca372a86820e18575_master.jpg" alt="" />
                    </div>
                    <div className="item">
                        <img className="img-fluid" src="http://file.hstatic.net/1000075078/file/1_3e629c9c22dc4ed5b5c8eab29a7005bf_master.jpg" alt="" />
                    </div>
                </div>
                {/* Left and right controls */}
                <a className="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span className="glyphicon glyphicon-chevron-left" />
                    <span className="sr-only">Previous</span>
                </a>
                <a className="right carousel-control" href="#myCarousel" data-slide="next">
                    <span className="glyphicon glyphicon-chevron-right" />
                    <span className="sr-only">Next</span>
                </a>
            </section>

        );
    }
}

export default HomeSlide;