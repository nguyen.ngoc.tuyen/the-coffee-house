import React, { Component } from 'react';

class Insta extends Component {
    render() {
        return (
            <section className="instagram">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-xs-12">
                            <h2 className="instagram_title line_after_heading section_heading">#ATTHECOFFEEHOUSE</h2>
                            <p className="instagram_content">Kết nối với Instagram (chính thức) của chúng tôi tại
          <span>@thecoffeehousevn.</span> Khi bạn đăng tải những khoảnh khắc của mình với The Coffee
          House, hãy nhớ <span>#atthecoffeehouse</span> để chúng tôi có thể chia sẻ lại trải nghiệm
          của bạn với mọi người. </p>
                        </div>
                        <div className="clearfix" />
                        <div className="instagram_lists flex_wrap display_flex" id="instafeed">
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Insta;