import React, { Component } from 'react';
import CustomerInfo from './CustomerInfo';
import './Cart.css';
import OrderInfo from './OrderInfo';

class Cart extends Component {
    render() {
        return (
            <div className="uk-container" style={{ padding: '15px', marginBottom: '40px' }}>
                <div uk-grid="" className="uk-grid-small uk-grid">
                    <CustomerInfo />
                    <OrderInfo />
                </div>
            </div>

        );
    }
}

export default Cart;