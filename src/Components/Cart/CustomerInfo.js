import React, { Component } from 'react';
import './Cart.css';

class CustomerInfo extends Component {
    render() {
        return (
            <div className="uk-width-2-3@l uk-width-1-1@m uk-first-column">
                <div>
                    <div className="uk-card uk-card-default uk-card-small uk-card-body uk-padding-remove-left uk-padding-remove-right uk-padding-remove-top">
                        <div className="uk-card-header">
                            <p className="uk-card-title">1. Xác nhận thông tin đơn hàng</p>
                        </div>
                        <div className="uk-card-body ">
                            <div uk-grid="" className="uk-grid-small uk-grid">
                                <div className="uk-width-1-1 uk-first-column">
                                    <img src="img/background/chuongtrinhthanhvien_7430ceea94f2429a8004c9f507631764.jpg" style={{ height: '300px', width: '100%', top: '0px', left: '0px' }} alt="" />
                                </div>
                                <div className="uk-width-1-1 uk-margin-top uk-grid-margin uk-first-column">
                                    <div className="uk-inline uk-width-1-1">
                                        <span className="uk-form-icon" style={{ color: 'rgb(112, 112, 112)' }}><img alt="" src="img/icon/location.png" /></span> <input type="text" placeholder="Nhập địa chỉ giao hàng" className="uk-input" />
                                    </div>
                                </div>
                                <div className="uk-width-1-2@l uk-width-1-2@m uk-margin-top uk-grid-margin uk-first-column">
                                    <div className="uk-inline uk-width-1-1"><span className="uk-form-icon" style={{ color: 'rgb(112, 112, 112)' }}><img alt="" src="img/icon/user.png" /></span> <input type="text" placeholder="Người nhận" className="uk-input" /></div>
                                </div>
                                <div className="uk-width-1-2@l uk-width-1-2@m uk-margin-top uk-grid-margin">
                                    <div className="uk-inline uk-width-1-1"><span className="uk-form-icon" style={{ color: 'rgb(112, 112, 112)' }}><img alt="" src="img/icon/phone.png" /></span> <input id="customer_phone" type="text" placeholder="Số điện thoại" className="uk-input" /></div>
                                </div>
                                <div className="uk-width-1-1@l uk-width-1-1@m uk-margin-top uk-grid-margin uk-first-column">
                                    <div className="uk-inline uk-width-1-1"><input type="text" placeholder="Ghi chú" className="uk-input" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="uk-margin-top uk-card uk-card-default uk-card-small uk-card-body uk-padding-remove-left uk-padding-remove-right uk-padding-remove-top">
                        <div className="uk-card-header">
                            <p className="uk-card-title">2. Hình thức thanh toán</p>
                        </div>
                        <div className="uk-card-body">
                            <div uk-grid="" className="uk-grid-small k-grid-collapse uk-grid">
                                <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@l tch-checkbox-payment-order uk-first-column">
                                    <label className="tch-cursor-pointer"><input type="radio" name="gender" defaultValue="1" defaultChecked className="uk-radio tch-radio" /> <span className="tch-text-checked"><img alt="" width={25} src="img/icon/cash.png" />
                                        Thanh toán
                                        khi giao hàng
                {/**/}</span></label></div>
                                <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@l  tch-checkbox-payment-order"><label className="tch-cursor-pointer"><input type="radio" name="gender" defaultValue="1" className="uk-radio tch-radio" /> <span className="tch-text-checked"><img alt="" width={25} src="img/icon/visa.png" />
                                    Visa/Master/JCB
                {/**/}</span></label></div>
                                <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@l tch-checkbox-payment-order uk-grid-margin uk-first-column">
                                    <label className="tch-cursor-pointer"><input type="radio" name="gender" defaultValue="2" className="uk-radio tch-radio" /> <span className="tch-text-checked"><img alt="" width={25} src="img/icon/atm.png" />
                                        Thẻ ATM nội
                                        địa
                {/**/}</span></label></div>
                                <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@l tch-checkbox-payment-order uk-grid-margin">
                                    <label className="tch-cursor-pointer"><input type="radio" name="gender" defaultValue="3" className="uk-radio tch-radio" /> <span className="tch-text-checked"><img alt="" width={25} src="img/icon/momo.png" />
                                        MoMo
                {/**/}</span></label></div>
                                <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@l tch-checkbox-payment-order uk-grid-margin uk-first-column">
                                    <label className="tch-cursor-pointer"><input type="radio" name="gender" defaultValue="4" className="uk-radio tch-radio" /> <span className="tch-text-checked"><img alt="" width={25} src="img/icon/zalo.png" />
                                        ZaloPay
                {/**/}</span></label></div>
                                <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@l tch-checkbox-payment-order uk-grid-margin">
                                    <label className="tch-cursor-pointer"><input type="radio" name="gender" defaultValue="5" className="uk-radio tch-radio" /> <span className="tch-text-checked"><img alt="" width={25} src="img/icon/airpay.png" />
                                        AirPay
                {/**/}</span></label></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default CustomerInfo;