import React, { Component } from 'react';

class OrderItem extends Component {
    render() {
        return (
            <div uk-grid="" className="uk-grid-small uk-padding-remove-bottom uk-grid" style={{ cursor: 'pointer' }}>
                <div className="tch-text-bold uk-text-center uk-width-auto uk-first-column">
                    <span className="uk-label uk-label-warning ">{this.props.quantity}</span></div>
                <dl className="uk-description-list uk-width-expand">
                    <dd className="tch-text-bold">{this.props.productTitle}</dd>
                </dl>
                <div className="uk-width-auto">{(this.props.productPrice * this.props.quantity).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + " đ"}</div>
            </div>
        );
    }
}

export default OrderItem;