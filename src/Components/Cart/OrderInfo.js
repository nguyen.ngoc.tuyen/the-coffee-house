import React, { Component } from 'react';
import OrderItem from './OrderItem';

class OrderInfo extends Component {
    showOrderInfo = () => {
        var shoppingCartItems = [];
        if (localStorage["cart"] != null) {
            shoppingCartItems = JSON.parse(localStorage["cart"].toString()); // Chuyển thông tin từ JSON trong localStorage sang mảng shoppingCartItems.

            return shoppingCartItems.map((value, key) => {
                return (
                    <OrderItem
                        key={key}
                        productId={value.productId}
                        productTitle={value.productTitle}
                        productPrice={value.productPrice}
                        quantity={value.quantity}
                    />
                )
            })
        }
        else return (<div >Bạn chưa có sản phẩm trong giỏ hàng</div>)
    }
    showPrice = () => {
        var shoppingCartItems = [];
        var prices = []
        if (localStorage["cart"] != null) {
            shoppingCartItems = JSON.parse(localStorage["cart"].toString()); // Chuyển thông tin từ JSON trong localStorage sang mảng shoppingCartItems.
            shoppingCartItems.forEach(element => {
                prices.push(JSON.parse(element.productPrice))
            })
            var sum = 0;
            for (var i = 0; i < prices.length; i++) {
                sum += prices[i];
            }
            console.log(sum);
            return (
                <div uk-grid="" className="uk-grid-small uk-grid">
                    <div className="uk-width-expand uk-first-column" style={{ marginTop: '5px' }}>
                        Tổng cộng
            </div>
                    <div className="uk-width-auto uk-text-large tch-text-bold">
                        {sum.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + " đ"}
            </div>
                </div>
            )
        }
    }
    render() {
        return (
            <div className="uk-width-1-3@l uk-width-1-1@m">
                <div>
                    <div className="uk-card uk-card-default uk-card-body uk-card-small uk-padding-remove" style={{ zIndex: 0 }}>
                        <div className="uk-card-header uk-visible@l"><button type="button" className="tch-text-bold uk-button uk-button-primary uk-width-1-1 uk-padding-remove-left uk-padding-remove-right">ĐẶT HÀNG</button></div>
                        {/**/}
                        <div>
                            <div>
                                <div className="uk-card-body">
                                    {this.showOrderInfo()}
                                </div>
                                {/**/}
                            </div>
                            <div className="uk-card-footer">
                                <div uk-grid="" className="uk-grid-small uk-padding-remove-bottom uk-grid">
                                    <div className="uk-width-expand uk-first-column">Vận chuyển</div>
                                    <div className="uk-width-auto">0&nbsp;₫</div>
                                </div>
                                {/**/}
                                {/**/}
                                <div uk-grid="" className="uk-grid-collapse uk-padding-remove-bottom uk-margin-top uk-grid">
                                    <div className="uk-width-expand uk-first-column"><input placeholder="Nhập mã ưu đãi tại đây" uk-form-width-medium="" type="text" className="uk-input" />
                                    </div>
                                    <div className="uk-width-auto"><button className="uk-button uk-button-default tch-text-white tch-text-bold uk-button-primary">ÁP
                DỤNG</button></div>
                                </div>
                                {/**/}
                            </div>
                            <div className="uk-card-footer">{this.showPrice()}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default OrderInfo;