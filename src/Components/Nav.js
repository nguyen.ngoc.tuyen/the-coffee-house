import React, { Component } from 'react';
import { Link } from "react-router-dom";

class Nav extends Component {
    render() {
        return (
            <div className="header" style={{ background: "black" }}>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div className="logo_wrap">
                                <a href="/" className="logo" title="the coffee house">
                                    <svg className="svg-logo">
                                        <use xmlnsXlink="http://www.w3.org/1999/xlink" xlinkHref="#logo" />
                                    </svg>
                                </a>
                                <h1 className="hidden"><a className="logo-name" href="/" title="The Coffee House">The Coffee House</a>
                                </h1>
                            </div>
                            <a className="visible-xs visible-sm" href="/" id="showmenu-mobile"><span /><span /><span /></a>
                        </div>
                        <div className="col-lg-9 col-md-9 col-sm-8 col-xs-12 header_menu">
                            <ul className="clearfix">
                                <li className="has_child">
                                    <a href="pages/cau-chuyen-thuong-hieu" title="Câu chuyện thương hiệu">Câu chuyện thương
              hiệu</a>
                                    <ul className="menu_child" id="menu_child_1_1">
                                        <li><a href="/" title="Câu chuyện thương hiệu">Trách nhiệm cộng đồng</a></li>
                                        <li><a href="/" title="Câu chuyện thương hiệu">Thông cáo báo chí</a></li>
                                    </ul>
                                </li>
                                <li className="has_child">
                                    <a href="pages/hanh-trinh-tu-nong-trai-den-ly-ca-phe" title="Chuyện cà phê">Chuyện cà
              phê</a>
                                    <ul className="menu_child" id="menu_child_2_2">
                                        <li><a href="pages/hanh-trinh-tu-nong-trai-den-ly-ca-phe" title="Chuyện cà phê">Hành
                  trình từ nông trại đến ly cà phê</a></li>
                                        <li><a href="pages/hat-ca-phe-the-coffee-house" title="Chuyện cà phê">Hạt cà phê the
                  coffee house</a></li>
                                        <li><a href="pages/nghe-thuat-pha-che" title="Chuyện cà phê">Nghệ thuật pha chế</a></li>
                                        <li><a href="blogs/workshop-ca-phe" title="Chuyện cà phê">Workshop cà phê</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <Link to="/menu">Thực đơn</Link>
                                </li>
                                <li>
                                    <a href="pages/rewards" title="Ưu đãi thành viên">Ưu đãi thành viên</a>
                                </li>
                                <li>
                                    <a href="blogs/news" title="Tin tức">Tin tức</a>
                                </li>
                                <li>
                                    <a href="http://tuyendung.thecoffeehouse.com/" title="Tuyển dụng" target="blank">Tuyển
              dụng</a>
                                </li>
                                <li>
                                    <a href="pages/our-stores" title="Cửa hàng">Cửa hàng</a>
                                </li>
                                <li className="search hidden-xs hidden-sm">
                                    <Link to="/cart" className="uk-icon"><span uk-icon="icon: cart; ratio: 1.2" className="uk-icon"><span
                                        className="uk-badge"></span><svg className="svg-cart" width="24" height="24" viewBox="0 0 20 20" 
                                            xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="7.3" cy="17.3" r="1.4"></circle>
                                            <circle cx="13.3" cy="17.3" r="1.4"></circle>
                                            <polyline fill="white" stroke="#000" points="0 2 3.2 4 5.3 12.5 16 12.5 18 6.5 8 6.5">
                                            </polyline>
                                        </svg></span></Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Nav;