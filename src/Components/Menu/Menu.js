import React, { Component } from 'react';
import MenuList from './MenuList/MenuList';
import MenuProducts from './MenuProduct/MenuProducts';
import MenuBanner from './MenuBanner';

class Menu extends Component {
    render() {
        return (
            <div className="main">
                <MenuBanner></MenuBanner>
                <div className="collection_menu_wrap">
                    <div className="container">
                        <div className="row">
                            <MenuList></MenuList>
                            <MenuProducts></MenuProducts>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Menu;