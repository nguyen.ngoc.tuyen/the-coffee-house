import React, { Component } from 'react';
import MenuBlock from './MenuBlock';
import { connect } from 'react-redux';

class MenuProducts extends Component {
    
    getCategory = () => {
        if(this.props.categoryData){
            return this.props.categoryData.map((value, key) => {
                return(
                    <MenuBlock
                    key = {key}
                    categoryTitle = {value.categoryTitle}
                    categoryId = {value.categoryId}
                    />
                )
            });
        }
    }
    render() {
        return (
            <div className="col-lg-9 col-md-9 col-sm-8 col-xs-12 border_right_before">
                {this.getCategory()}
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        categoryData: state.categoryData
    }
  }
  const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(MenuProducts);