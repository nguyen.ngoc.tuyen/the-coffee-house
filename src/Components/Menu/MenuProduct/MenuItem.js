import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';

class MenuItem extends Component {
    getInfo = () => {
        this.props.getProductInfo(this.props.product)
    }
    addToCart = () => {
        // Định nghĩa một mảng các phần tử sẽ bỏ vào giỏ hàng
        var shoppingCartItems = [];
        var quantity = 1
        var item = {
            productId: this.props.productId,
            productTitle: this.props.productTitle,
            productPrice: this.props.productPrice,
            quantity: quantity
        };
        // Kiểm tra nếu đã có localStorage["cart"] hay chưa?
        if (localStorage["cart"] != null) {
            shoppingCartItems = JSON.parse(localStorage["cart"].toString())

            var exists = false
            if (shoppingCartItems.length > 0) {
                shoppingCartItems.forEach(element => {
                    if (element.productId === item.productId) {
                        element.quantity++;
                        exists = true;
                        return false;
                    }
                });
            }
            // Nếu mặt hàng chưa tồn tại trong giỏ hàng thì bổ sung vào mảng
            if (!exists) {
                shoppingCartItems.push(item);
            }

            // Lưu thông tin vào localStorage
            localStorage["cart"] = JSON.stringify(shoppingCartItems); // Chuyển thông tin mảng shoppingCartItems sang JSON trước khi lưu vào localStorage   
        }
        else {
            shoppingCartItems.push(item);
            // Lưu thông tin vào localStorage
            localStorage["cart"] = JSON.stringify(shoppingCartItems); // Chuyển thông tin mảng shoppingCartItems sang JSON trước khi lưu vào localStorage
        }
    }
    render() {
        return (
            <div className="menu_item">
                <div className="menu_item_image">
                    <Link to={'/product/' + this.props.productId} title="BẠC SỈU" onClick={() => { this.getInfo() }} ><img src={this.props.productImage} alt="bac siu" /></Link>
                </div>
                <div className="menu_item_info bg_white">
                    <h3><Link to="/product" title="BẠC SỈU" onClick={() => { this.getInfo() }} >{this.props.productTitle}</Link></h3>
                    <div className="price_product_item">{this.props.productPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + " đ"}</div>
                    <Link to="/cart" className="menu_item_action animate_btn" title="Mua Ngay" onClick={() => { this.addToCart() }}>Mua ngay</Link>
                    <a className="menu_item_action_view" onClick={() => { this.addToCart() }}>Thêm vào giỏ hàng</a>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getProductInfo: (productInfo) => {
            dispatch({ type: "GET_PRODUCT_INFO", productInfo })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuItem);