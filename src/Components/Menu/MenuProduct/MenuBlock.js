import React, { Component } from 'react';
import MenuItem from './MenuItem';
import { connect } from 'react-redux';

class MenuBlock extends Component {

    getProducts = () => {
        if (this.props.productData) {
            return this.props.productData.map((value, key) => {
                if (value.productCategory === this.props.categoryId) {
                    return (
                        <MenuItem
                            key={key}
                            product={value}
                            productId={value.productId}
                            productTitle={value.productTitle}
                            productImage={value.productImage}
                            productPrice={value.productPrice}
                            productTitleDes={value.productTitleDes}
                            productDescription={value.productDescription}
                            productCategory={value.productCategory}
                        />
                    )
                }
                return true
            });
        }
    }
    render() {
        return (
            <div className="block_menu_item" id={this.props.categoryId}>
                <h3 className="block_menu_item_title"><span className="line_after_heading section_heading">{this.props.categoryTitle}</span></h3>
                <div className="flex_wrap display_flex menu_lists">
                    {this.getProducts()}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        productData: state.productData
    }
  }
  const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(MenuBlock);