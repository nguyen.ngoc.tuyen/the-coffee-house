import React, { Component } from 'react';

class MenuListItem extends Component {
    render() {
        return (
            <li><a className="menu_scroll_link" href={"#" + this.props.categoryId}>{this.props.categoryTitle}</a></li>
        );
    }
}

export default MenuListItem;