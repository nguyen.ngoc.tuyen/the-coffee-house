import React, { Component } from 'react';
import './MenuList.css';
import MenuListItem from './MenuListItem';
import { connect } from 'react-redux';

class MenuList extends Component {
    
    getCategory = () => {
        if(this.props.categoryData){
            return this.props.categoryData.map((value, key) => {
                return(
                    <MenuListItem
                    key = {key}
                    categoryTitle = {value.categoryTitle}
                    categoryId = {value.categoryId}
                    />
                )
            });
        }
    }
    render() {
        return (
            <div className="col-lg-3 col-md-3 col-sm-4 col-xs-12 stikySidebar">
                <aside className="sidebar_menu">
                    <ul>
                        {this.getCategory()}
                    </ul>
                </aside>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        categoryData: state.categoryData
    }
  }
  const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(MenuList);