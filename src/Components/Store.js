
var redux = require('redux')
const categoryInitialState = {
    categoryData: [],
    productData: [],
    productInfo: {}
}
const category = (state = categoryInitialState, action) => {
    switch (action.type) {
        case "GET_CATEGORY":
            return {...state, categoryData: action.getCategory}
        case "GET_PRODUCT":
            return {...state, productData: action.getProduct}
        case "GET_PRODUCT_INFO":
            return {...state, productInfo: action.productInfo}
        default:
            return state
    }
}
var store = redux.createStore(category)
export default store;