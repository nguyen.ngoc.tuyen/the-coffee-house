import React, { Component } from 'react';
import './../App.css';
import {
  BrowserRouter as Router
} from "react-router-dom";
import Nav from './Nav';
import Footer from './Footer';
import NavURL from '../Router/NavURL';
import { category } from './../Firebase/FirebaseConnect';
import { connect } from 'react-redux';
import * as firebase from 'firebase';


class App extends Component {

  UNSAFE_componentWillMount() {
    category.on('value', (categories) => {
      var tempCategory = []
      var tempProducts = []

      categories.forEach(element => {
        const id = element.key
        const categoryTitle = element.val().categoryTitle
        // const products = element.val().products
        firebase.database().ref('data/' + 'category/' + id + "/products").on('value', (prods) => {
          // console.log("prods", prods);

          prods.forEach(e => {
            // console.log(e.key);
            tempProducts.push({
              productId: e.key,
              productTitle: e.val().productTitle,
              productImage: e.val().productImage,
              productPrice: e.val().productPrice,
              productTitleDes: e.val().productTitleDes,
              productDescription: e.val().productDescription,
              productCategory: id
            })

          })
        });
        
        tempCategory.push({
          categoryId: id,
          categoryTitle: categoryTitle
        })
      });

      this.props.getCategory(tempCategory)
      this.props.getProduct(tempProducts)
    })
  }

  render() {
    return (
      <Router>
        <div className="App">
          <Nav></Nav>
          <NavURL></NavURL>
          <Footer></Footer>
        </div>
      </Router>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getCategory: (getCategory) => {
      dispatch({ type: "GET_CATEGORY", getCategory })
    },
    getProduct: (getProduct) => {
      dispatch({ type: "GET_PRODUCT", getProduct })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
