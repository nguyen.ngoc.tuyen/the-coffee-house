import React, { Component } from 'react';
import Breadcrumb from './Breadcrumb';
import './Css/Product.css';
import Info from './Info';
import Description from './Description';
import Related from './Related';
import { connect } from 'react-redux';

class Product extends Component {
    render() {
        return (
            <div className="main">
                <Breadcrumb
                    productTitle={this.props.productInfo.productTitle}
                    productCategory={this.props.productInfo.productCategory} />
                <Info
                    productId={this.props.productInfo.productId}
                    productTitle={this.props.productInfo.productTitle}
                    productImage={this.props.productInfo.productImage}
                    productPrice={this.props.productInfo.productPrice} />
                <Description
                    productTitleDes={this.props.productInfo.productTitleDes}
                    productDescription={this.props.productInfo.productDescription} />
                <Related 
                    productId={this.props.productInfo.productId}
                    productCategory={this.props.productInfo.productCategory} />
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        productInfo: state.productInfo
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Product);