import React, { Component } from 'react';
import { Link } from "react-router-dom";

class Info extends Component {
    
    addToCart = () => {
        
        // Định nghĩa một mảng các phần tử sẽ bỏ vào giỏ hàng
        var shoppingCartItems = [];
        var quantity = 1
        var item = {
            productId: this.props.productId,
            productTitle: this.props.productTitle,
            productPrice: this.props.productPrice,
            quantity: quantity
        }
        // Kiểm tra nếu đã có localStorage["cart"] hay chưa?
        if (localStorage["cart"] != null) {
            shoppingCartItems = JSON.parse(localStorage["cart"].toString())

            var exists = false
            if (shoppingCartItems.length > 0) {
                shoppingCartItems.forEach(element => {
                    if (element.id === item.id) {
                        element.quantity++;
                        exists = true;
                        return false;
                    }
                });
            }
            // Nếu mặt hàng chưa tồn tại trong giỏ hàng thì bổ sung vào mảng
            if (!exists) {
                shoppingCartItems.push(item);
            }

            // Lưu thông tin vào localStorage
            localStorage["cart"] = JSON.stringify(shoppingCartItems); // Chuyển thông tin mảng shoppingCartItems sang JSON trước khi lưu vào localStorage   
        }
        else {
            shoppingCartItems.push(item);
            // Lưu thông tin vào localStorage
            localStorage["cart"] = JSON.stringify(shoppingCartItems); // Chuyển thông tin mảng shoppingCartItems sang JSON trước khi lưu vào localStorage
        }
    }
    render() {
        return (
            <div className="product-info">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="product_info_wrap flex_wrap display_flex">
                                <div id="img_product">
                                    <img className="product_featured_image" src={this.props.productImage} alt="tra xoai macchiato" />
                                </div>
                                <div className="info_product display_flex align-items-center">
                                    <div className="info_product_wrap">
                                        <h1 className="info_product_title line_after_heading">{this.props.productId}</h1>
                                        <form id="add_item_form" action="cart/add" method="post" className="variants clearfix">
                                            <div className="select clearfix" style={{ display: 'none' }}>
                                                <select id="product-select" name="id" style={{ display: 'none' }}>
                                                    <option value={1043736631}>Default Title - 55,000 đ</option>
                                                </select>
                                                <input type="hidden" defaultValue id="inventory_qty" />
                                            </div>
                                            <div className="product_price">{this.props.productPrice.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + " đ"}</div>
                                            <div className="btn_action"><Link to="/cart" title="Mua ngay" onClick={() => { this.addToCart() }}>MUA NGAY</Link></div>
                                            <p className="delivery_hotline">Gọi giao hàng tận nơi <a href="tel:18006936" title="Hotline: 1800 6936">1800 6936</a></p>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Info;