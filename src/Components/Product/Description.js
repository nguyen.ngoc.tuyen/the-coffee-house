import React, { Component } from 'react';
import './Css/Product.css';

class Description extends Component {
    render() {
        return (
            <div className="product_description">
                <div className="block_des block_des_1" style={{ textAlign: 'center' }} data-mce-style="text-align: center;">
                    <div className="block_container">
                        <h3>{this.props.productTitleDes}</h3>
                        <p>{this.props.productDescription}</p>
                    </div>
                </div>
            </div>

        );
    }
}

export default Description;