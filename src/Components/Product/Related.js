import React, { Component } from 'react';
import MenuItem from '../Menu/MenuProduct/MenuItem';
import { connect } from 'react-redux';

class Related extends Component {
    getProductRelated = () => {
        var count = 0
        return this.props.productData.map((value, key) => {
            if (value.productId !== this.props.productId) {
                if (value.productCategory === this.props.productCategory) {
                    if (count <= 2) {
                        count++
                        return (
                            <MenuItem
                                key={key}
                                product={value}
                                productId={value.productId}
                                productTitle={value.productTitle}
                                productImage={value.productImage}
                                productPrice={value.productPrice}
                                productTitleDes={value.productTitleDes}
                                productDescription={value.productDescription}
                                productCategory={value.productCategory}
                            />
                        )
                    }
                }
            }
            return true;
        });
    }
    render() {
        return (
            <div id="product-related">
                <div className="container_flex">
                    <div className="row_flex">
                        <h3 className="related_product_title">Có thể bạn thích</h3>
                        <div className="flex_wrap display_flex menu_lists">
                            {this.getProductRelated()}
                        </div>
                        <div className="list_product_related flex_wrap display_flex menu_lists" />
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        productData: state.productData
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Related);