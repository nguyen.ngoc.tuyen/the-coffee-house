import React, { Component } from 'react';
import { Link } from "react-router-dom";

class Breadcrumb extends Component {
	render() {
		return (
			<div className="container">
				<div className="row">
					<div className="col-md-12 ">
						<ol className="breadcrumb breadcrumb-arrow" itemScope itemType="http://schema.org/BreadcrumbList">
							<li itemProp="itemListElement" itemScope itemType="http://schema.org/ListItem"><Link to="/menu" target="_self"><span itemProp="name">Menu</span></Link>
								<meta itemProp="position" content={1} />
							</li>
							<li itemProp="itemListElement" itemScope itemType="http://schema.org/ListItem"><Link to="/menu" target="_self" itemProp="item"><span itemProp="name">{this.props.productCategory}</span></Link>
								<meta itemProp="position" content={2} />
							</li>
							<li itemProp="itemListElement" itemScope itemType="http://schema.org/ListItem" className="active"><span itemProp="name"> {this.props.productTitle}</span>
								<meta itemProp="position" content={2} />
							</li>
						</ol>
					</div>
				</div>
			</div>
		);
	}
}

export default Breadcrumb;