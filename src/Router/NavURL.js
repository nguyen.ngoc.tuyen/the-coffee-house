import React, { Component } from 'react';
import {
    Switch,
    Route
} from "react-router-dom";
import Menu from '../Components/Menu/Menu';
import Home from '../Components/Home/Home';
import Product from '../Components/Product/Product';
import Cart from '../Components/Cart/Cart';

class NavURL extends Component {
    render() {
        return (
                <div>
                    <Switch>
                        <Route path="/menu">
                            <Menu />
                        </Route>
                        <Route path="/product">
                            <Product />
                        </Route>
                        <Route path="/cart">
                            <Cart />
                        </Route>
                        <Route path="/">
                            <Home />
                        </Route>
                    </Switch>
                </div>
        );
    }
}

export default NavURL;