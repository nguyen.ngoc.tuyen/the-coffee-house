import * as firebase from 'firebase';

var firebaseConfig = {
    apiKey: "AIzaSyArWHw5WmjNMfkXLP2Nn0V6v6WupY-kG7U",
    authDomain: "thecoffeehouse-7f415.firebaseapp.com",
    databaseURL: "https://thecoffeehouse-7f415.firebaseio.com",
    projectId: "thecoffeehouse-7f415",
    storageBucket: "thecoffeehouse-7f415.appspot.com",
    messagingSenderId: "304161627450",
    appId: "1:304161627450:web:e279cf601c898e04954403",
    measurementId: "G-WE4FVHRV6K"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();
export const category = firebase.database().ref('data/' + 'category')